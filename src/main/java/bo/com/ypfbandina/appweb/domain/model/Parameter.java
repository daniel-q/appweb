package bo.com.ypfbandina.appweb.domain.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Getter
@Setter
@Entity(name = "PARAMETERS")
public class Parameter {

    @Id
    @GeneratedValue
    public int id;

    public String key;

    public String value;

    public String description;

    public Parameter() {
        this("", "");
    }

    public Parameter(String key, String value) {
        this.key = key;
        this.value = value;
    }
}
