package bo.com.ypfbandina.appweb.domain.services;

import bo.com.ypfbandina.appweb.domain.interfaces.services.HelloWorldService;

public class HelloWorldServiceImpl implements HelloWorldService {
    public String greeting() {
        return "Hola mundo SPA";
    }
}
