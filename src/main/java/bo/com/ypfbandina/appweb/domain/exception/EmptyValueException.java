package bo.com.ypfbandina.appweb.domain.exception;

public class EmptyValueException extends Exception {

    public EmptyValueException() {
        super("Value cannot be empty or null.");
    }
}
