package bo.com.ypfbandina.appweb.domain.interfaces.repository;

import bo.com.ypfbandina.appweb.domain.model.Parameter;

public interface ParameterRepository extends GenericRepository<Parameter> {

}
