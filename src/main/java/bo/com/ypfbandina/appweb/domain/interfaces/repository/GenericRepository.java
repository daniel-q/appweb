package bo.com.ypfbandina.appweb.domain.interfaces.repository;

import java.util.List;

public interface GenericRepository<T> {

    List<T> getAll();

    List<T> getByQuery();

    T getBy();

    void create(T entity);

    void update(T entity);

    void delete(T entity);
}
