package bo.com.ypfbandina.appweb.domain.interfaces.services;

import bo.com.ypfbandina.appweb.domain.exception.EmptyValueException;
import bo.com.ypfbandina.appweb.domain.exception.ExistingEntityException;
import bo.com.ypfbandina.appweb.domain.model.Parameter;

import java.util.List;

public interface ParameterService {

    List<Parameter> getAll();

    Parameter getByKey(String key);

    void create(Parameter parameter) throws EmptyValueException, ExistingEntityException;
}
