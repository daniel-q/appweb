package bo.com.ypfbandina.appweb.domain.services;

import bo.com.ypfbandina.appweb.domain.exception.EmptyValueException;
import bo.com.ypfbandina.appweb.domain.exception.ExistingEntityException;
import bo.com.ypfbandina.appweb.domain.interfaces.repository.ParameterRepository;
import bo.com.ypfbandina.appweb.domain.interfaces.services.ParameterService;
import bo.com.ypfbandina.appweb.domain.model.Parameter;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

public class ParameterServiceImpl implements ParameterService {

    private ParameterRepository parameterRepository;

    @Inject
    public ParameterServiceImpl(ParameterRepository parameterRepository) {

        this.parameterRepository = parameterRepository;
    }

    public List<Parameter> getAll() {

        //return this.parameterRepository.getAll(); //TODO implement this method rightly

        Parameter[] parameters = {
                new Parameter("parameter-1", "value-1"),
                new Parameter("parameter-2", "value-2"),
                new Parameter("parameter-3", "value-3"),
                new Parameter("parameter-4", "value-4")
        };

        return Arrays.asList(parameters);
    }

    public Parameter getByKey(String key) {

        //return this.parameterRepository.getBy(); //TODO implement this method rightly

        return new Parameter("parameter-x", "value-x");
    }

    public void create(Parameter parameter) throws EmptyValueException, ExistingEntityException {

        validateEntity(parameter);

        this.parameterRepository.create(parameter);
    }

    private void validateEntity(Parameter parameter) throws EmptyValueException, ExistingEntityException {

        if ("".equals(parameter.key))
            throw new EmptyValueException();

        if ("".equals(parameter.value))
            throw new EmptyValueException();

        Parameter dbParameter = this.parameterRepository.getBy();

        if (dbParameter != null)
            throw new ExistingEntityException();
    }
}
