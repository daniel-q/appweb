package bo.com.ypfbandina.appweb.domain.exception;

public class ExistingEntityException extends Exception {

    public ExistingEntityException() {
        super("Entity already exists.");
    }
}
