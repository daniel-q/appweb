package bo.com.ypfbandina.appweb.rest;

import bo.com.ypfbandina.appweb.domain.interfaces.services.HelloWorldService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/hello-world")
@Stateless
public class HelloWorldResource {

    @Inject
    private HelloWorldService helloWorldService;

    @GET
    @Path("/greeting")
    public String greeting(){
        return helloWorldService.greeting();
    }
}
