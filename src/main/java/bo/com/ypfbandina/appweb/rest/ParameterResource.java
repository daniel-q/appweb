package bo.com.ypfbandina.appweb.rest;

import bo.com.ypfbandina.appweb.domain.interfaces.services.ParameterService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@Path("/parameters")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON + ";CHARSET=UTF-8")
public class ParameterResource {

    @Inject
    private ParameterService parameterService;

    @GET
    @Path("/")
    public Response getAll() {

        return Response.ok(parameterService.getAll()).build();
    }

    @GET
    @Path("{key}")
    public Response getByValue(@PathParam("key") String key) {

        return Response.ok(parameterService.getByKey(key)).build();
    }
}
