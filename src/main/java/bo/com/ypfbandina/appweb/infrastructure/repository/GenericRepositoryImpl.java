package bo.com.ypfbandina.appweb.infrastructure.repository;

import java.util.List;

public class GenericRepositoryImpl<T> {

    public List<T> getAll() {
        return null;
    }

    public List<T> getByQuery() {
        return null;
    }

    public T getBy() {
        return null;
    }

    public void create(T entity) {

    }

    public void update(T entity) {

    }

    public void delete(T entity) {

    }
}
