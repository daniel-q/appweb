package bo.com.ypfbandina.appweb.infrastructure.repository;

import bo.com.ypfbandina.appweb.domain.interfaces.repository.ParameterRepository;
import bo.com.ypfbandina.appweb.domain.model.Parameter;

public class ParameterRepositoryImpl
        extends GenericRepositoryImpl<Parameter>
        implements ParameterRepository {

}
